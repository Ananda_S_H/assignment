angular.module('login', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('login').config(function($stateProvider) {

    /* Add New States Above */

    $stateProvider.state('login',{
    	url:'/login',
    	templateUrl:'login/partial/login/login.html',
    	controller:'LoginCtrl'
    })
    .state('login.signup',{
            url:'/signup',
            templateUrl:'login/partial/login/signup.html'
        })
    .state('login.in',{
            url:'/in',
            templateUrl:'login/partial/login/in.html'
        })
    .state('login.dashboard',{
            url:'/dashboard',
            templateUrl:'login/partial/login/dashboard.html'
        })
    .state('login.list',{
            url:'/list',
            templateUrl:'login/partial/login/list.html'
        })
    .state('login.details',{
        url:'/details',
        templateUrl:'login/partial/login/details.html'
    })

});

